FROM markhobson/maven-chrome

WORKDIR /app

COPY ./src src/
COPY ./pom.xml pom.xml
COPY ./open-api-spec open-api-spec
#COPY ./trello_config_prod.json trello_config_prod.json
COPY ./trello_web_ui_config_prod.json trello_web_ui_config_prod.json

#ENV CONFIG_PATH=/app/trello_config_prod.json
ENV MAVEN_OPTS="-Xmx3000m"
ENV WEB_UI_CONFIG_PATH=/app/trello_web_ui_config_prod.json

RUN mvn clean package -DskipTests

ENTRYPOINT ["mvn", "test", "-P lesson-1011-parallel-ui"]
