package ru.levelp.at.spring.sample.app.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.levelp.at.spring.sample.app.api.model.UserRequest;
import ru.levelp.at.spring.sample.app.api.model.UserResponse;
import ru.levelp.at.spring.sample.app.model.User;
import ru.levelp.at.spring.sample.app.repository.UserRepository;
import ru.levelp.at.spring.sample.app.service.UserService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    @Qualifier("databaseUserRepository")
    private final UserRepository userRepository;

    @Override
    public void add(String username, String password) {
        userRepository.add(username, password);
    }

    @Override
    public String getPassword(String username) {
        return userRepository.getPassword(username);
    }

    @Override
    public UserResponse add(UserRequest request) {
        final var id = UUID.randomUUID().toString();
        final var createdDate = LocalDateTime.now();
        final var user = new User(id, request.getUsername(), request.getPassword(), createdDate);

        userRepository.add(user);

        return new UserResponse(user.id(), user.username(), user.password(), user.created());
    }

    @Override
    public UUID addClearRest(UserRequest request) {
        final var id = UUID.randomUUID().toString();
        final var createdDate = LocalDateTime.now();
        final var user = new User(id, request.getUsername(), request.getPassword(), createdDate);

        userRepository.add(user);

        return UUID.fromString(id);
    }

    @Override
    public List<UserResponse> findAll() {
        final var dbUsersList = userRepository.findAll();

        return dbUsersList.stream()
                .map(user -> new UserResponse(user.id(), user.username(), user.password(), user.created()))
                .toList();
    }

    @Override
    public UserResponse findById(String id) {
        final var user = userRepository.findById(UUID.fromString(id));
        return new UserResponse(user.id(), user.username(), user.password(), user.created());
    }
}
