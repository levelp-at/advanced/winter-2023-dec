package ru.levelp.at.spring.sample.app.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.levelp.at.spring.sample.app.model.User;
import ru.levelp.at.spring.sample.app.repository.UserRepository;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository("databaseUserRepository")
// @Primary
@RequiredArgsConstructor
public class DatabaseUserRepository implements UserRepository {

    private static final String SELECT_ALL = "SELECT * FROM users";
    private static final String SELECT_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String SELECT_BY_USERNAME = "SELECT * FROM users WHERE username = ?";
    private static final String INSERT = "INSERT INTO users VALUES(:id, :username, :password, :created)";

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public void add(String username, String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getPassword(String username) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(User user) {
        final var params = Map.of(
            "id", user.id(),
            "username", user.username(),
            "password", user.password(),
            "created", user.created()
        );
        namedParameterJdbcTemplate.update(INSERT, params);
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SELECT_ALL, DataClassRowMapper.newInstance(User.class));
    }

    @Override
    public User findById(UUID id) {
        return jdbcTemplate.queryForObject(SELECT_BY_ID, DataClassRowMapper.newInstance(User.class), id.toString());
    }

    @Override
    public User findByUsername(String username) {
        return jdbcTemplate.queryForObject(SELECT_BY_USERNAME, DataClassRowMapper.newInstance(User.class), username);
    }
}
