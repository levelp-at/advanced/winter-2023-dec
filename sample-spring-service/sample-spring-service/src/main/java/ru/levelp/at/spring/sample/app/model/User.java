package ru.levelp.at.spring.sample.app.model;

import java.time.LocalDateTime;
import java.util.UUID;

// db_columns has snake_case name
// db_column_name: username -> java_class_field: username
// db_column_name: first_name -> java_class_field: firstName
public record User(
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
