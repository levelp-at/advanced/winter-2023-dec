package ru.levelp.at.spring.sample.app.service;

import ru.levelp.at.spring.sample.app.api.model.UserRequest;
import ru.levelp.at.spring.sample.app.api.model.UserResponse;

import java.util.List;
import java.util.UUID;

public interface UserService {

    void add(String username, String password);

    String getPassword(String username);

    UserResponse add(UserRequest request);

    UUID addClearRest(UserRequest request);

    List<UserResponse> findAll();

    UserResponse findById(String id);
}
