package ru.levelp.at.spring.sample.app.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//public record UserRequest(
//    String username,
//    String password
//) {
//}
@Data
public class UserRequest {

    private String username;
    private String password;
}
