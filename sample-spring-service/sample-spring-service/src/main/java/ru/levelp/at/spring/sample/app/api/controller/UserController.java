package ru.levelp.at.spring.sample.app.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.levelp.at.spring.sample.app.api.model.UserRequest;
import ru.levelp.at.spring.sample.app.api.model.UserResponse;
import ru.levelp.at.spring.sample.app.service.UserService;
import java.net.URI;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user")
    public void add(@RequestParam String username, @RequestParam String password) {
        userService.add(username, password);
    }

    @GetMapping("/get_user")
    public String getPassword(@RequestParam String username) {
        return "Password for username %s is %s".formatted(username, userService.getPassword(username));
    }

    @PostMapping("/users")
    public ResponseEntity<UserResponse> add(@RequestBody UserRequest request) {
        final var response = userService.add(request);

        return ResponseEntity.status(HttpStatusCode.valueOf(201))
            .body(response);
    }

    @PostMapping("/users_clear_rest")
    public ResponseEntity<String> addClearRest(@RequestBody UserRequest request) {
        final var response = userService.addClearRest(request);

        return ResponseEntity.created(URI.create("/users/%s".formatted(response.toString()))).build();
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> getAll() {
        final var response = userService.findAll();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserResponse> getById(@PathVariable String id) {
        final var response = userService.findById(id);

        return ResponseEntity.ok(response);
    }
}
