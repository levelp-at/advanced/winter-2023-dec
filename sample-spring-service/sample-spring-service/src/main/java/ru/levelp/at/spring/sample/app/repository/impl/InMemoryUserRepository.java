package ru.levelp.at.spring.sample.app.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import ru.levelp.at.spring.sample.app.model.User;
import ru.levelp.at.spring.sample.app.repository.UserRepository;

@Repository
// @Primary
public class InMemoryUserRepository implements UserRepository {

    private static final Map<String, String> STORAGE = new HashMap<>();

    @Override
    public void add(String username, String password) {
        STORAGE.put(username, password);
    }

    @Override
    public String getPassword(String username) {
        return STORAGE.get(username);
    }

    @Override
    public void add(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public User findById(UUID id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public User findByUsername(String username) {
        throw new UnsupportedOperationException();
    }
}
