package ru.levelp.at.spring.sample.app.api.model;

import java.time.LocalDateTime;
import java.util.UUID;

public record UserResponse(
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
