package ru.levelp.at.spring.sample.app.repository;

import ru.levelp.at.spring.sample.app.model.User;

import java.util.List;
import java.util.UUID;

public interface UserRepository {

    void add(String username, String password);

    String getPassword(String username);

    void add(User user);

    List<User> findAll();

    User findById(UUID id);

    User findByUsername(String username);
}
