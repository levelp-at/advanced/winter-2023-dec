plugins {
    java
    id("org.springframework.boot") version "3.2.2"
//    id("org.springframework.cloud.contract") version "4.1.1"
}

group = "ru.levelp.at.advance"
version = "1.0.1"

allprojects {
    apply(plugin = "java")
    apply(plugin = "org.springframework.boot")

    java.sourceCompatibility = JavaVersion.VERSION_17
    java.targetCompatibility = JavaVersion.VERSION_17

    repositories {
        mavenLocal()
        mavenCentral()
    }

    tasks.test {
        useJUnitPlatform()
        testLogging {
            showStandardStreams = true
        }
    }
}
