package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return 201 and response body when request is correct"
    request {
        method POST()
        headers {
            contentType(applicationJson())
        }
        url("/users") {
            body(
                username: "user",
                password: "password"
            )
        }
    }
    response {
        status CREATED()
        headers {
            contentType(applicationJson())
        }
        body(
            username: "user",
            password: "password"
        )
    }
}
