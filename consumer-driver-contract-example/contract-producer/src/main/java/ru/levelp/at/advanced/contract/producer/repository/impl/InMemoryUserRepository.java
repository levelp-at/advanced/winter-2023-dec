package ru.levelp.at.advanced.contract.producer.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;
import org.springframework.stereotype.Repository;
import ru.levelp.at.advanced.contract.producer.model.User;
import ru.levelp.at.advanced.contract.producer.repository.UserRepository;

@Repository
public class InMemoryUserRepository implements UserRepository {

    private static final Map<UUID, User> STORAGE = new HashMap<>();

    @Override
    public void add(User user) {
        STORAGE.put(UUID.fromString(user.id()), user);
    }

    @Override
    public List<User> findAll() {
        return STORAGE.values().stream().toList();
    }

    @Override
    public User findById(UUID id) {
        final var user = STORAGE.get(id);
        if (user == null) {
            throw new NoSuchElementException("Не удалось найти пользователя с id: %s".formatted(id.toString()));
        }

        return user;
    }

    @Override
    public User findByUsername(String username) {
        return STORAGE.values()
            .stream()
            .filter(user -> user.username().equals(username))
            .findFirst()
            .orElseThrow(() -> new NoSuchElementException("Не удалось найти пользователя с username: %s".formatted(username)));
    }
}
