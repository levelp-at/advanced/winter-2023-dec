package ru.levelp.at.advanced.contract.producer.api.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.levelp.at.advanced.contract.producer.api.model.UserRequest;
import ru.levelp.at.advanced.contract.producer.api.model.UserResponse;

public interface UserController {

    @PostMapping("/users")
    public ResponseEntity<UserResponse> add(@RequestBody UserRequest request);

    @PostMapping("/users_clear_rest")
    public ResponseEntity<String> addClearRest(@RequestBody UserRequest request);

    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> getAll();

    @GetMapping("/users/{id}")
    public ResponseEntity<UserResponse> getById(@PathVariable String id);
}
