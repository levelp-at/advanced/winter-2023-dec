package ru.levelp.at.advanced.contract.producer.api.model;

import lombok.Data;

@Data
public class UserRequest {

    private String username;
    private String passwordS;
}
