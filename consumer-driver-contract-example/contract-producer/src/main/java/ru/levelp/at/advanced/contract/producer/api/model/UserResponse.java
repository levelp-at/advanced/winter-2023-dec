package ru.levelp.at.advanced.contract.producer.api.model;

import java.time.LocalDateTime;
import java.util.UUID;

public record UserResponse(
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
