package ru.levelp.at.advanced.contract.producer.service;


import ru.levelp.at.advanced.contract.producer.api.model.UserRequest;
import ru.levelp.at.advanced.contract.producer.api.model.UserResponse;
import java.util.List;
import java.util.UUID;

public interface UserService {

    UserResponse add(UserRequest request);

    UUID addClearRest(UserRequest request);

    List<UserResponse> findAll();

    UserResponse findById(String id);
}
