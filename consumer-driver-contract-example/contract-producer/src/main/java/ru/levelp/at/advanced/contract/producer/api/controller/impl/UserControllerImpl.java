package ru.levelp.at.advanced.contract.producer.api.controller.impl;

import java.net.URI;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.levelp.at.advanced.contract.producer.api.controller.UserController;
import ru.levelp.at.advanced.contract.producer.api.model.UserRequest;
import ru.levelp.at.advanced.contract.producer.api.model.UserResponse;
import ru.levelp.at.advanced.contract.producer.service.UserService;

@RestController
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Override
    public ResponseEntity<UserResponse> add(@RequestBody UserRequest request) {
        final var response = userService.add(request);

        return ResponseEntity.status(HttpStatusCode.valueOf(201))
            .body(response);
    }

    @Override
    public ResponseEntity<String> addClearRest(@RequestBody UserRequest request) {
        final var response = userService.addClearRest(request);

        return ResponseEntity.created(URI.create("/users/%s".formatted(response.toString()))).build();
    }

    @Override
    public ResponseEntity<List<UserResponse>> getAll() {
        final var response = userService.findAll();

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UserResponse> getById(@PathVariable String id) {
        final var response = userService.findById(id);

        return ResponseEntity.ok(response);
    }
}
