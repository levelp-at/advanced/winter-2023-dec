package ru.levelp.at.advanced.contract.producer.repository;


import ru.levelp.at.advanced.contract.producer.model.User;
import java.util.List;
import java.util.UUID;

public interface UserRepository {

    void add(User user);

    List<User> findAll();

    User findById(UUID id);

    User findByUsername(String username);
}
