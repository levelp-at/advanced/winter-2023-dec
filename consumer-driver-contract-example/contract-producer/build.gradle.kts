import org.springframework.cloud.contract.verifier.config.TestFramework

plugins {
    `maven-publish`
    `java-library`
    id("org.springframework.cloud.contract") version "3.1.10"
}

description = "contract-producer"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:3.2.2")
    implementation("org.springframework.boot:spring-boot-starter-data-rest:3.2.2")
    compileOnly("org.projectlombok:lombok:1.18.30")
    annotationProcessor("org.projectlombok:lombok:1.18.30")

    testImplementation("org.springframework.cloud:spring-cloud-contract-verifier:3.1.10")
    testImplementation("org.apache.groovy:groovy:4.0.16")
    testImplementation("io.rest-assured:rest-assured:5.4.0") {
        exclude(group = "org.codehaus.groovy", module = "groovy-xml")
    }
    testImplementation("io.rest-assured:spring-mock-mvc:5.4.0")
    testImplementation("org.junit.platform:junit-platform-launcher:1.10.1")

//    testImplementation(platform("org.junit:junit-bom:5.9.1"))
//    testImplementation("org.junit.jupiter:junit-jupiter")
}

contracts {
    packageWithBaseClasses = "ru.levelp.at.advance.contract.producer.test"
    contractsDslDir = project.file("${project.projectDir}/src/test/resources/contracts")
    baseClassForTests = "ru.levelp.at.advance.contract.producer.test.BaseTestClass"
    baseClassMappings {
        baseClassMapping(".*", "ru.levelp.at.advance.contract.producer.test.BaseTestClass")
    }
    testFramework = TestFramework.JUNIT5
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            this.groupId = project.rootProject.group.toString()
            artifactId = description
            this.version = project.rootProject.version.toString()

            from(components["java"])

        }

        create<MavenPublication>("stubs") {
            this.groupId = project.rootProject.group.toString()
            artifactId = "${project.name}-stubs"
            this.version = project.rootProject.version.toString()

            from(components["java"])
        }

//        register("stubs", MavenPublication::class) {
//            from(components["contracts"])
//        }
    }

    repositories {
        maven {
            isAllowInsecureProtocol = true
            url = uri("http://localhost:8088/nexus/content/repositories/releases/")

            credentials {
                username = "admin"
                password = "admin123"
            }
        }
    }
}
