rootProject.name = "consumer-driver-contract-example"

pluginManagement {
    repositories {
        mavenCentral()
    }
}

include("contract-producer")
include("contract-consumer")
