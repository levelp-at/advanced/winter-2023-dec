dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:3.2.2")
    implementation("org.springframework.boot:spring-boot-starter-data-rest:3.2.2")
    compileOnly("org.projectlombok:lombok:1.18.30")
    annotationProcessor("org.projectlombok:lombok:1.18.30")

    testImplementation("org.springframework.cloud:spring-cloud-contract-wiremock:4.1.1")
    testImplementation("org.springframework.cloud:spring-cloud-contract-stub-runner:4.1.1")
    testImplementation("org.apache.groovy:groovy:4.0.18")
    testImplementation("io.rest-assured:rest-assured:5.4.0") {
        exclude(group="org.codehaus.groovy", module = "groovy-xml")
    }
    testImplementation("org.junit.platform:junit-platform-launcher:1.10.1")

    // testImplementation(project(":contract-producer"))

    testImplementation("ru.levelp.at.advance:contract-producer-stubs:1.0.0")

//    testImplementation(platform("org.junit:junit-bom:5.9.1"))
//    testImplementation("org.junit.jupiter:junit-jupiter")
}
