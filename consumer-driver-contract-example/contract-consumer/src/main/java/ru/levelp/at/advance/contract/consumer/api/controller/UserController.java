package ru.levelp.at.advance.contract.consumer.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.levelp.at.advance.contract.consumer.api.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.api.model.UserResponse;

public interface UserController {

    @PostMapping("/users")
    UserResponse add(@RequestBody UserRequest user);
}
