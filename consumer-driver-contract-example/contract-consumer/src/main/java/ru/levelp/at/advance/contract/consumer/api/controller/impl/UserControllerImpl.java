package ru.levelp.at.advance.contract.consumer.api.controller.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.levelp.at.advance.contract.consumer.api.controller.UserController;
import ru.levelp.at.advance.contract.consumer.api.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.api.model.UserResponse;
import ru.levelp.at.advance.contract.consumer.service.UserService;

@RestController
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Override
    public UserResponse add(UserRequest user) {
        return userService.add(user);
    }
}
