package ru.levelp.at.advance.contract.consumer.client.model;

public record UserRequest(
    String username,
    String password
) {
}
