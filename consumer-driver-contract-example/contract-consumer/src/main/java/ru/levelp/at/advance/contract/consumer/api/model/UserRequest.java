package ru.levelp.at.advance.contract.consumer.api.model;

public record UserRequest(
    String username
) {
}
