package ru.levelp.at.advance.contract.consumer.client.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.levelp.at.advance.contract.consumer.client.UserApiClient;
import ru.levelp.at.advance.contract.consumer.client.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.client.model.UserResponse;

@Component
@RequiredArgsConstructor
public class UserApiClientImpl implements UserApiClient {

    private final RestTemplate restTemplate;

    @Override
    public UserResponse add(UserRequest request) {
        final var headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");

        UserResponse response = restTemplate
            .postForObject("http://localhost:8080/users", new HttpEntity<>(request, headers), UserResponse.class);

        return response;
    }
}
