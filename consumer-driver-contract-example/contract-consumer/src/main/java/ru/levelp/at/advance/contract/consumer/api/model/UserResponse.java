package ru.levelp.at.advance.contract.consumer.api.model;

import java.time.LocalDateTime;
import java.util.UUID;

public record UserResponse(
    UUID id,
    String username,
    String password,
    LocalDateTime created
) {
}
