package ru.levelp.at.advance.contract.consumer.client;

import ru.levelp.at.advance.contract.consumer.client.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.client.model.UserResponse;

public interface UserApiClient {

    UserResponse add(UserRequest request);
}
