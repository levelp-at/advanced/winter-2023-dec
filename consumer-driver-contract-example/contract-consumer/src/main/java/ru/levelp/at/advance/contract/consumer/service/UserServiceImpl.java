package ru.levelp.at.advance.contract.consumer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.levelp.at.advance.contract.consumer.api.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.api.model.UserResponse;
import ru.levelp.at.advance.contract.consumer.client.UserApiClient;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserApiClient userApiClient;

    @Override
    public UserResponse add(UserRequest request) {
        final var apiClientRequest = new ru.levelp.at.advance.contract.consumer.client.model.UserRequest(request.username(), "password");

        final var apiClientResponse = userApiClient.add(apiClientRequest);

        return new UserResponse(apiClientResponse.id(), apiClientResponse.username(), apiClientResponse.password(), apiClientResponse.created());
    }
}
