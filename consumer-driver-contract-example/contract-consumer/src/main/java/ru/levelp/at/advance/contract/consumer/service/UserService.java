package ru.levelp.at.advance.contract.consumer.service;

import ru.levelp.at.advance.contract.consumer.api.model.UserRequest;
import ru.levelp.at.advance.contract.consumer.api.model.UserResponse;

public interface UserService {

    UserResponse add(UserRequest request);
}
