package ru.level.at.advance.contract.consumer.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
@AutoConfigureStubRunner(
    ids = {"ru.levelp.at.advance:contract-producer:+:stubs:8084"},
    stubsMode = StubsMode.LOCAL
)
@ContextConfiguration
class ExampleTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void name() throws Exception {
        ResultActions result =
            mockMvc.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON)
                                                  .content("""
                                                      {
                                                        "username": "789789456"
                                                      }
                                                      """));

        System.out.println(result.andReturn().getResponse().getContentAsString());
    }

    @Configuration
    @ComponentScan(
        basePackages = "ru.levelp.at.advanced.contract")
    static class ContractTestConfig {}
}
