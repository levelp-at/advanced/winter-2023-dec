package ru.levelp.at.advance.lesson1011;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

class ReadResourceTest {

    @Test
    void test() {
        try {
            String string = Paths.get(this.getClass().getResource("/env/application-local.properties")
                                          .toURI()).toAbsolutePath().toString();
            System.out.printf("file path: %s%n", string);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
