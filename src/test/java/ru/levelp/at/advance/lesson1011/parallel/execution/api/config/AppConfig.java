package ru.levelp.at.advance.lesson1011.parallel.execution.api.config;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {"ru.levelp.at.advance.lesson1011"})
@PropertySource({"classpath:/env/application-${ENV:local}.properties"})
public class AppConfig {

    @Bean
    public RequestSpecification requestSpecification(
        @Value("${api.url}") String url,
        @Value("${api.port}") int port
    ) {
        return new RequestSpecBuilder()
            .log(LogDetail.ALL)
            .setContentType(ContentType.JSON)
            .setBaseUri(url)
            .setPort(port)
            .build();
    }

    @Bean
    public ResponseSpecification responseSpecification() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .build();
    }
}
