package ru.levelp.at.advance.lesson1011.parallel.execution.api.test;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.levelp.at.advance.lesson1011.parallel.execution.api.client.UserApiClient;
import ru.levelp.at.advance.lesson1011.parallel.execution.api.config.AppConfig;
import ru.levelp.at.advance.lesson1011.parallel.execution.api.model.UserRequest;
import ru.levelp.at.advance.lesson1011.parallel.execution.api.model.UserResponse;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AppConfig.class})
class UserServiceTest {

    private static final Faker FAKER = new Faker();

    @Autowired
    private UserApiClient userApiClient;

    @Autowired
    private ResponseSpecification responseSpecification;

    @Test
    void createUser1Test() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        userApiClient.addUser(request)
                     .then()
                     .spec(responseSpecification)
                     .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    void createUser4Test() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        userApiClient.addUser(request)
                     .then()
                     .spec(responseSpecification)
                     .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    void createUser2Test() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        userApiClient.addUser(request)
                     .then()
                     .spec(responseSpecification)
                     .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    void createUser3Test() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        userApiClient.addUser(request)
                     .then()
                     .spec(responseSpecification)
                     .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    void createUserWithHttpRequestTest() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        final var postResponse = userApiClient.addUser(request)
                                              .then()
                                              .spec(responseSpecification)
                                              .statusCode(HttpStatus.SC_CREATED)
                                              .extract()
                                              .as(UserResponse.class);

        final var getResponse = userApiClient.getUser(postResponse.id())
                                             .then()
                                             .spec(responseSpecification)
                                             .extract()
                                             .as(UserResponse.class);

        assertThat(getResponse).as("Проверка создания и получения объекта").isEqualTo(postResponse);
    }
}
