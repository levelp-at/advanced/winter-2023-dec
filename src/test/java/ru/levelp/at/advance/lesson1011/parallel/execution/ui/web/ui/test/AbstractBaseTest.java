package ru.levelp.at.advance.lesson1011.parallel.execution.ui.web.ui.test;

import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.AfterEach;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.config.WebUiConfiguration;

public abstract class AbstractBaseTest {

    protected WebUiConfiguration config;

    //    @BeforeEach
    //    void setUp() {
    //        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    //        config = WebUiConfigurationProvider.getConfig();
    //
    //        Configuration.browser = "firefox";
    //        Configuration.timeout = 15000;
    //        Configuration.remote = "http://localhost:4444/wd/hub";
    //        // Configuration.headless = config.headless();
    //
    //        Selenide.open(config.url());
    //    }

    @AfterEach
    void tearDown() {
        Selenide.closeWebDriver();
    }
}
