package ru.levelp.at.advance.lesson1011.parallel.execution.ui.web.ui.test;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.Assertions.assertThat;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.github.javafaker.Faker;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.config.WebUiConfigurationProvider;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.service.page.object.AtlassianAuthorizationPage;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.service.page.object.TrelloBoardPage;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.service.page.object.TrelloBoardsPage;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.service.page.object.TrelloBoardsPage.CreateMenu;
import ru.levelp.at.advance.lesson1011.parallel.execution.ui.service.page.object.TrelloMainPage;

class CreateBoardTest extends AbstractBaseTest {

    @BeforeEach
    void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        config = WebUiConfigurationProvider.getConfig();

        Configuration.timeout = 15000;
        // Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.headless = config.headless();

        Selenide.open(config.url());
    }

    @Test
    void testCreateNewBoard1() {
        loginStep(config.username(), config.password());

        var boardName = new Faker().funnyName().name();
        System.out.println(boardName);
        createBoard(boardName);

        step("проверка названия созданной доски", () -> {
            var trelloBoardPage = new TrelloBoardPage();
            var actualName = trelloBoardPage.boardTitle.shouldBe(visible).text();
            assertThat(actualName)
                .as("ПроверЯем название доски")
                .isEqualTo(boardName);
        });
    }

    @Test
    void testCreateNewBoard2() {
        loginStep(config.username(), config.password());

        var boardName = new Faker().funnyName().name();
        System.out.println(boardName);
        createBoard(boardName);

        step("проверка названия созданной доски", () -> {
            var trelloBoardPage = new TrelloBoardPage();
            var actualName = trelloBoardPage.boardTitle.shouldBe(visible).text();
            assertThat(actualName)
                .as("ПроверЯем название доски")
                .isEqualTo(boardName);
        });
    }

    @Test
    void testCreateNewBoard3() {
        loginStep(config.username(), config.password());

        var boardName = new Faker().funnyName().name();
        System.out.println(boardName);
        createBoard(boardName);

        step("проверка названия созданной доски", () -> {
            var trelloBoardPage = new TrelloBoardPage();
            var actualName = trelloBoardPage.boardTitle.shouldBe(visible).text();
            assertThat(actualName)
                .as("ПроверЯем название доски")
                .isEqualTo(boardName);
        });
    }

    @Step("Логинимся на сайт")
    private void loginStep(final String username, final String password) {
        var mainPage = new TrelloMainPage();
        mainPage.loginButton.as("Кнопка 'Login'").shouldBe(visible).click();

        var authPage = new AtlassianAuthorizationPage();
        authPage.usernameTextBox().shouldBe(visible).sendKeys(username);
        authPage.continueButton().shouldBe(visible).click();
        authPage.passwordTextBox().shouldBe(visible).sendKeys(password);
        authPage.loginButton().shouldBe(visible).click();
    }

    @Step("Создаём доску")
    private void createBoard(final String boardName) {
        var boardsPage = new TrelloBoardsPage();
        boardsPage.getCreateButton().shouldBe(visible).click();
        boardsPage.selectMenu(CreateMenu.CREATE_BOARD);
        boardsPage.getCreateBoardTitleTextBox().shouldBe(visible).sendKeys(boardName);
        boardsPage.getCreateBoardButton().shouldBe(enabled).click();
    }
}
