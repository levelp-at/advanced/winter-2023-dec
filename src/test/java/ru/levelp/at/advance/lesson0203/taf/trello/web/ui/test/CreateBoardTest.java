package ru.levelp.at.advance.lesson0203.taf.trello.web.ui.test;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;
import ru.levelp.at.advance.lesson0203.taf.service.page.object.AtlassianAuthorizationPage;
import ru.levelp.at.advance.lesson0203.taf.service.page.object.TrelloBoardPage;
import ru.levelp.at.advance.lesson0203.taf.service.page.object.TrelloBoardsPage;
import ru.levelp.at.advance.lesson0203.taf.service.page.object.TrelloBoardsPage.CreateMenu;
import ru.levelp.at.advance.lesson0203.taf.service.page.object.TrelloMainPage;

class CreateBoardTest extends AbstractBaseTest {

    @Test
    void testCreateNewBoard() {
        loginStep(config.username(), config.password());

        var boardName = new Faker().funnyName().name();
        System.out.println(boardName);
        createBoard(boardName);

        step("проверка названия созданной доски", () -> {
            var trelloBoardPage = new TrelloBoardPage();
            var actualName = trelloBoardPage.boardTitle.shouldBe(visible).text();
            assertThat(actualName)
                .as("ПроверЯем название доски")
                .isEqualTo(boardName);
        });
    }

    @Step("Логинимся на сайт")
    private void loginStep(final String username, final String password) {
        var mainPage = new TrelloMainPage();
        mainPage.loginButton.as("Кнопка 'Login'").shouldBe(visible).click();

        var authPage = new AtlassianAuthorizationPage();
        authPage.usernameTextBox().shouldBe(visible).sendKeys(username);
        authPage.continueButton().shouldBe(visible).click();
        authPage.passwordTextBox().shouldBe(visible).sendKeys(password);
        authPage.loginButton().shouldBe(visible).click();
    }

    @Step("Создаём доску")
    private void createBoard(final String boardName) {
        var boardsPage = new TrelloBoardsPage();
        boardsPage.getCreateButton().shouldBe(visible).click();
        boardsPage.selectMenu(CreateMenu.CREATE_BOARD);
        boardsPage.getCreateBoardTitleTextBox().shouldBe(visible).sendKeys(boardName);
        boardsPage.getCreateBoardButton().shouldBe(enabled).click();
    }
}
