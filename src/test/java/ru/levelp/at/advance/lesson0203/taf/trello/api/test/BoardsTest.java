package ru.levelp.at.advance.lesson0203.taf.trello.api.test;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import ru.levelp.at.advance.lesson0203.taf.api.client.BoardsApiClient;
import ru.levelp.at.advance.lesson0203.taf.config.RequestSpecificationConfig;

class BoardsTest {

    private static final Faker FAKER = new Faker();

    @Test
    void testCreate() {
        // сгенернировать название доски
        final var boardName = step("Создаем имя новвой доски", () -> FAKER.funnyName().name());
        // final String boardName = step("Создаем имя новвой доски", () -> FAKER.funnyName().name());

        // создать доску
        final var response = step("Отправляем запрос", () ->
            BoardsApiClient.of(RequestSpecificationConfig.defaultSpecification()).createBoard(boardName))
            .then()
            .log().all()
            .statusCode(HttpStatus.SC_OK);

        // проверить что доска создалась
        step("Проверка создания доски", () -> {
            final var actualBoardResponse = step("Получение созданной доски по id", () -> {
                final String actualBoardId = response.extract().body().jsonPath().get("id");
                return BoardsApiClient.of(RequestSpecificationConfig.defaultSpecification())
                                      .getBoard(actualBoardId)
                                      .then()
                                      .log().all();
            });

            final String actualName = actualBoardResponse.extract().body().jsonPath().get("name");

            assertThat(actualName)
                .as("Проверка, что доска создалась с нужным именем")
                .isEqualTo(boardName);
        });
    }
}
