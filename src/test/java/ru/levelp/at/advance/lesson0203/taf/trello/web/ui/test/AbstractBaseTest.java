package ru.levelp.at.advance.lesson0203.taf.trello.web.ui.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import ru.levelp.at.advance.lesson0203.taf.config.WebUiConfiguration;
import ru.levelp.at.advance.lesson0203.taf.config.WebUiConfigurationProvider;

public abstract class AbstractBaseTest {

    protected WebUiConfiguration config;

    @BeforeEach
    void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        config = WebUiConfigurationProvider.getConfig();

        Configuration.timeout = 15000;
        Configuration.headless = config.headless();

        Selenide.open(config.url());
    }

    @AfterEach
    void tearDown() {
        Selenide.closeWebDriver();
    }
}
