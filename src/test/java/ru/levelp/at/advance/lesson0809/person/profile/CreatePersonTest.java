package ru.levelp.at.advance.lesson0809.person.profile;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import ru.levelp.at.advance.lesson0809.person.profile.model.PersonEntity;
import ru.levelp.at.advance.lesson0809.person.profile.queries.PersonQueries;
import ru.levelp.at.advance.person.profile.ApiClient;
import ru.levelp.at.advance.person.profile.api.PeopleApi;
import ru.levelp.at.advance.person.profile.model.CreatePersonData;
import ru.levelp.at.advance.person.profile.model.IdentityData;
import ru.levelp.at.advance.person.profile.model.PersonResponse;
import ru.levelp.at.advance.person.profile.model.PersonRole;

@TestInstance(Lifecycle.PER_CLASS)
class CreatePersonTest {

    private static final Faker FAKER = new Faker();

    private PeopleApi peopleApiClient;

    @BeforeAll
    void beforeAll() {
        peopleApiClient = new ApiClient()
            //            .setBasePath("http://localhost:8080/srv-person-profile")
            .setBasePath("http://api.person.profile.levelp.ru/srv-person-profile")
            .buildClient(PeopleApi.class);
    }

    @Test
    void createPersonTest() {
        final var createPersonData = new CreatePersonData()
            .role(PersonRole.ADMINISTRATOR)
            .email(FAKER.internet().emailAddress())
            .phoneNumber(FAKER.phoneNumber().phoneNumber())
            .identity(new IdentityData()
                .firstName(FAKER.name().firstName())
                .lastName(FAKER.name().lastName()));

        final PersonResponse person = peopleApiClient.createPerson(createPersonData);
        final PersonEntity actualDbObject = PersonQueries.getPerson(person.getData().getId());

        assertThat(actualDbObject)
            .as("Проверка создания записи в таблице БД")
            .isNotNull();

        System.out.println(actualDbObject);
    }
}
