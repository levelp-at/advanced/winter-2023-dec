package ru.levelp.at.advance.lesson0607.spring.test;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import io.restassured.specification.ResponseSpecification;
import java.time.LocalDateTime;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.levelp.at.advance.lesson0607.spring.api.client.UserApiClient;
import ru.levelp.at.advance.lesson0607.spring.api.model.UserRequest;
import ru.levelp.at.advance.lesson0607.spring.api.model.UserResponse;
import ru.levelp.at.advance.lesson0607.spring.config.AppConfig;
import ru.levelp.at.advance.lesson0607.spring.database.model.UserEntity;
import ru.levelp.at.advance.lesson0607.spring.database.queries.UserQueries;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AppConfig.class})
class UserServiceWithDatabaseTest {

    private static final Faker FAKER = new Faker();

    @Autowired
    private UserApiClient userApiClient;

    @Autowired
    private ResponseSpecification responseSpecification;

    @Autowired
    private UserQueries userQueries;

    @Test
    void createUserWithDatabaseRequestTest() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        final var createdDate = LocalDateTime.now();
        final var postResponse = userApiClient.addUser(request)
                                              .then()
                                              .spec(responseSpecification)
                                              .statusCode(HttpStatus.SC_CREATED)
                                              .extract()
                                              .as(UserResponse.class);

        final var user = userQueries.getUser(postResponse.id());

        SoftAssertions.assertSoftly(softly -> {
            assertThat(user)
                .as("Проверка создания и получения объекта")
                .usingRecursiveComparison()
                .ignoringExpectedNullFields()
                .isEqualTo(new UserEntity(null, request.username(), request.password(), null));
            assertThat(user.created())
                .as("Проверка даты создания")
                .isAfter(createdDate);
        });
    }
}
