package ru.levelp.at.advance.lesson0607.spring.test;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.levelp.at.advance.lesson0607.spring.api.client.UserApiClient;
import ru.levelp.at.advance.lesson0607.spring.api.model.UserRequest;
import ru.levelp.at.advance.lesson0607.spring.api.model.UserResponse;
import ru.levelp.at.advance.lesson0607.spring.config.AppConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AppConfig.class})
class UserServiceTest {

    private static final Faker FAKER = new Faker();

    @Autowired
    private UserApiClient userApiClient;

    @Autowired
    private ResponseSpecification responseSpecification;

    @Test
    void createUserTest() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        userApiClient.addUser(request)
                .then()
                .spec(responseSpecification)
                .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    void createUserWithHttpRequestTest() {
        final var request = new UserRequest(FAKER.funnyName().name(), FAKER.internet().password());

        final var postResponse = userApiClient.addUser(request)
                .then()
                .spec(responseSpecification)
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .as(UserResponse.class);

        final var getResponse = userApiClient.getUser(postResponse.id())
                .then()
                .spec(responseSpecification)
                .extract()
                .as(UserResponse.class);

        assertThat(getResponse).as("Проверка создания и получения объекта").isEqualTo(postResponse);
    }
}
