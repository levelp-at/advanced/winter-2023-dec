package ru.levelp.at.advance.lesson15.wiremock;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static io.restassured.RestAssured.given;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

@WireMockTest // (httpPort = 8080)
public class SampleWiremockTest {

    @Test
    public void sampleTest(WireMockRuntimeInfo wmRuntimeInfo) {
        stubFor(post("/users")
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("""
                        {
                            "id": "1231231287423891",
                            "username": "Nameddddd",
                            "password": "ppppppp",
                            "created": "2024-02-29T19:58:30"
                        }
                    """)));

        given()
            .log().all()
            .contentType(ContentType.JSON)
            .body("""
                {
                    "asa": "ppp"
                }
                """)
            .when()
            .post("http://localhost:%d/users".formatted(wmRuntimeInfo.getHttpPort()))
            .then()
            .log().all()
            .statusCode(200);
    }
}
