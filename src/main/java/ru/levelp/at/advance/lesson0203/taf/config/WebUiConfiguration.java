package ru.levelp.at.advance.lesson0203.taf.config;

public record WebUiConfiguration(
    String url,
    String username,
    String password,
    Boolean headless
) {
}
