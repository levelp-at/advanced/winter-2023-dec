package ru.levelp.at.advance.lesson0203.taf.service.page.object;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.support.FindBy;

public class TrelloBoardsPage {

    @FindBy(xpath = "//*[@data-testid='header-create-menu-button']")
    private SelenideElement createButton;

    @FindBy(xpath = "//*[@data-testid='header-create-menu-popover']//*[starts-with(@data-testid, 'header-create-')]")
    private ElementsCollection createBoardMenu;

    @FindBy(xpath = "//*[@data-testid='create-board-title-input']")
    private SelenideElement createBoardTitleTextBox;

    @FindBy(xpath = "//*[@data-testid='create-board-submit-button']")
    private SelenideElement createBoardButton;

    public TrelloBoardsPage() {
        Selenide.page(this);
    }

    public void selectMenu(CreateMenu createMenu) {
        for (SelenideElement elem : createBoardMenu) {
            if (elem.text().contains(createMenu.getName())) {
                elem.click();
                break;
            }
        }
    }

    public SelenideElement getCreateButton() {
        return createButton;
    }

    public SelenideElement getCreateBoardTitleTextBox() {
        return createBoardTitleTextBox;
    }

    public SelenideElement getCreateBoardButton() {
        return createBoardButton;
    }


    @RequiredArgsConstructor
    public enum CreateMenu {
        CREATE_BOARD("Create board"),
        START_WITH_A_TEMPLATE("Start with a template"),
        CREATE_WORKSPACE("Create workspace");

        @Getter
        final String name;
    }
}
