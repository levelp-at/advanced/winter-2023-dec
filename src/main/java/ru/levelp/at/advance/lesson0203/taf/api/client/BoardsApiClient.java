package ru.levelp.at.advance.lesson0203.taf.api.client;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Map;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class BoardsApiClient {

    private static final String BOARDS_ENDPOINT = "/boards";
    private static final String BOARD_ENDPOINT = BOARDS_ENDPOINT + "/{id}";

    private final RequestSpecification requestSpecification;

    public Response createBoard(final String name) {
        return given()
            .spec(requestSpecification)
            .queryParams(Map.of("name", name))
            .when()
            .post(BOARDS_ENDPOINT)
            .andReturn();
    }

    public Response getBoard(final String id) {
        return given()
            .spec(requestSpecification)
            .pathParam("id", id)
            .when()
            .get(BOARD_ENDPOINT)
            .andReturn();
    }
}
