package ru.levelp.at.advance.lesson0203.taf.service.page.object;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class AtlassianAuthorizationPage {

    public SelenideElement usernameTextBox() {
        return Selenide.$x("//*[@id='username']");
    }

    public SelenideElement continueButton() {
        return Selenide.$x("//button[@id='login-submit']");
    }

    public SelenideElement passwordTextBox() {
        return Selenide.$x("//*[@id='password']");
    }

    public SelenideElement loginButton() {
        return Selenide.$x("//button[@id='login-submit']");
    }
}
