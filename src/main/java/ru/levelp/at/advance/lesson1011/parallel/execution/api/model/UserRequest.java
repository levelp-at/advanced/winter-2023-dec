package ru.levelp.at.advance.lesson1011.parallel.execution.api.model;

public record UserRequest(
    String username,
    String password
) {
}
