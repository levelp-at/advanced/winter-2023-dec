package ru.levelp.at.advance.lesson1011.parallel.execution.api.database.model;

import java.time.LocalDateTime;
import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.core.RowMapper;

public record UserEntity(
    String id,
    String username,
    String password,
    LocalDateTime created
) {

    public static final RowMapper<UserEntity> MAPPER = DataClassRowMapper.newInstance(UserEntity.class);
}
