package ru.levelp.at.advance.lesson1011.parallel.execution.ui.config;

public record WebUiConfiguration(
    String url,
    String username,
    String password,
    Boolean headless
) {
}
