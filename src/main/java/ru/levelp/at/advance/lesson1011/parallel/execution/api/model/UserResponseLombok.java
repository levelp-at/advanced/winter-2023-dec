package ru.levelp.at.advance.lesson1011.parallel.execution.api.model;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class UserResponseLombok {

    private String id;
    private String username;
    private String password;
    private LocalDateTime created;
}
/* ==
public record UserResponse (
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
*/
