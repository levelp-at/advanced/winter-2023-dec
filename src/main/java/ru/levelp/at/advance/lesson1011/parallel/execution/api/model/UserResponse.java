package ru.levelp.at.advance.lesson1011.parallel.execution.api.model;

import java.time.LocalDateTime;

public record UserResponse(
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
