package ru.levelp.at.advance.lesson1011.selenoid.ui.config;

public record WebUiConfiguration(
    String url,
    String username,
    String password,
    Boolean headless
) {
}
