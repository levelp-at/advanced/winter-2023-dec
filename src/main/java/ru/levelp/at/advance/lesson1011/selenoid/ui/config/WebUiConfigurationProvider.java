package ru.levelp.at.advance.lesson1011.selenoid.ui.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class WebUiConfigurationProvider {

    @SneakyThrows
    public WebUiConfiguration getConfig() {
        final var configFilePath = System.getenv("WEB_UI_CONFIG_PATH");

        if (configFilePath == null) {
            throw new IllegalArgumentException("Please set up WEB_UI_CONFIG_PATH variable to env variables");
        }

        return new ObjectMapper().readValue(new File(configFilePath), WebUiConfiguration.class);
    }
}
