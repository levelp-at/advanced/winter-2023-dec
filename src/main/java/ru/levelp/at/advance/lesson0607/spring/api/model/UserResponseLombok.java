package ru.levelp.at.advance.lesson0607.spring.api.model;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class UserResponseLombok {

    private String id;
    private String username;
    private String password;
    private LocalDateTime created;
}
/* ==
public record UserResponse (
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
*/
