package ru.levelp.at.advance.lesson0607.spring.api.client;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.levelp.at.advance.lesson0607.spring.api.model.UserRequest;

@Component
@RequiredArgsConstructor
public class UserApiClient {

    private static final String USERS_ENDPOINT = "/users";
    private static final String USER_ENDPOINT =  "%s/{id}".formatted(USERS_ENDPOINT);


    private final RequestSpecification requestSpecification;

    public Response addUser(UserRequest request) {
        return given()
            .spec(requestSpecification)
            .body(request)
            .when()
            .post(USERS_ENDPOINT)
            .andReturn();
    }

    public Response getUsers() {
        return given()
            .spec(requestSpecification)
            .when()
            .get(USERS_ENDPOINT)
            .andReturn();
    }

    public Response getUser(String id) {
        return given()
            .spec(requestSpecification)
            .pathParam("id", id)
            .when()
            .get(USER_ENDPOINT)
            .andReturn();
    }
}
