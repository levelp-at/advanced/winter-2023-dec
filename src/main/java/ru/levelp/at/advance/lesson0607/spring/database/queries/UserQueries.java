package ru.levelp.at.advance.lesson0607.spring.database.queries;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.levelp.at.advance.lesson0607.spring.database.model.UserEntity;

@Repository
@RequiredArgsConstructor
public class UserQueries {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserEntity getUser(final String id) {
        final var sql = """
            SELECT * FROM users WHERE id = ?
            """;

        return jdbcTemplate.queryForObject(sql, UserEntity.MAPPER, id);
    }
}
