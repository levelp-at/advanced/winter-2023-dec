package ru.levelp.at.advance.lesson0607.spring.api.model;

public record UserRequest(
    String username,
    String password
) {
}
