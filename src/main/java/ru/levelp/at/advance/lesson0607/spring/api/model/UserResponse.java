package ru.levelp.at.advance.lesson0607.spring.api.model;

import java.time.LocalDateTime;

public record UserResponse(
    String id,
    String username,
    String password,
    LocalDateTime created
) {
}
