package ru.levelp.at.advance.lesson0809.person.profile.queries;

import ru.levelp.at.advance.lesson0809.person.profile.dao.PersonProfileDao;
import ru.levelp.at.advance.lesson0809.person.profile.model.PersonEntity;

public class PersonQueries {

    public static PersonEntity getPerson(String id) {
        final var sql = """
            SELECT * FROM person WHERE id = ?::uuid
            """;
        return PersonProfileDao.getInstance().getJdbcTemplate()
                               .queryForObject(sql, PersonEntity.MAPPER, id);
    }
}
