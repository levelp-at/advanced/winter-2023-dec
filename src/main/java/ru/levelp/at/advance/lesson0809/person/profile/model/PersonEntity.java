package ru.levelp.at.advance.lesson0809.person.profile.model;

import java.time.LocalDate;
import java.util.UUID;
import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.core.RowMapper;

public record PersonEntity(
    UUID id,
    String email,
    String phoneNumber,
    String role,
    String firstName,
    String lastName,
    String middleName,
    String gender,
    LocalDate dateOfBirth,
    String placeOfBirth,
    String placeOfWork,
    Integer passportSeries,
    Integer passportNumber,
    String placeOfIssue,
    LocalDate dateOfIssue,
    String departmentCode,
    String street,
    Integer houseNumber,
    Integer houseBuilding,
    String houseLetter,
    Integer flat,
    String city,
    String postalCode
) {

    public static final RowMapper<PersonEntity> MAPPER = DataClassRowMapper.newInstance(PersonEntity.class);
}
