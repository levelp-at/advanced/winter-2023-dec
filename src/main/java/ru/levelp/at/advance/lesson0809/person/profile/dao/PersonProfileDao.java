package ru.levelp.at.advance.lesson0809.person.profile.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class PersonProfileDao {

    private static PersonProfileDao instance;

    @Getter
    private final JdbcTemplate jdbcTemplate;

    @Getter
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private PersonProfileDao() {
        final var config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/person_profile");
        config.setUsername("person_profile");
        config.setPassword("password");

        final var dataSource = new HikariDataSource(config);
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public static PersonProfileDao getInstance() {
        if (instance == null) {
            instance = new PersonProfileDao();
        }
        return instance;
    }
}
